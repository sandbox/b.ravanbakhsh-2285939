CONTENTS OF THIS FILE
---------------------
* Requirements
* Installation
* Configuration
* Maintainers

REQUIREMENTS
------------
A Payline account.
Also this module requires the following module:
* Views (https://drupal.org/project/ubercart)

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information
  
CONFIGURATION
-------------
1. Enable the payment method.
2. Enter your Merchant ID into the payment configuration page in in Store > 
	Configuration > Payment methods.
 
MAINTAINERS
-----------
Current maintainer
 Behzad R Bakhsh - https://drupal.org/user/1413464